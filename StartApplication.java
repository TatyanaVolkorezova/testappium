package test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class StartApplication {

	public static void main(String[] args)  throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub
		File classpathRoot = new File(System.getProperty("user.dir"));
		//File appDir = new File(classpathRoot, "Users/tatyanavolkorezova/IdeaProjects/apps");
		File app = new File(classpathRoot, "app-dev-debug.apk");
	
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("device", "Android");
	//	capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability("deviceName", "Tatyana Volkorezova_G3");
	//	capabilities.setCapability("platformVersion", "5.0");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("appPackage", "com.fieldlens.android");
	//	capabilities.setCapability("appActivity", ".activities.DemoActivity");

		AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		Thread.sleep(10000);
		driver.quit();

	}
	
	

}
