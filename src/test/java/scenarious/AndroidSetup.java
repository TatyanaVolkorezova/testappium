package scenarious;

import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by tatyanavolkorezova on 18.01.17.
 */
public class AndroidSetup {

     public static AndroidDriver driver;

   public static AndroidDriver prepeareAndroidForAppium() throws MalformedURLException{

       File classpathRoot = new File(System.getProperty("user.dir"));
       File app = new File(classpathRoot, "app-dev-debug.apk");
      // File appDir = new File("/Users/tatyanavolkorezova/IdeaProjects/TestAppium");
      // File app = new File(appDir,"app-dev-debug.apk");

       DesiredCapabilities capabilities = new DesiredCapabilities();
       capabilities.setCapability("device", "Android");
       //	capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
       capabilities.setCapability("deviceName", "Tatyana Volkorezova_G3");
       //	capabilities.setCapability("platformVersion", "5.0");
       capabilities.setCapability("platformName", "Android");
       capabilities.setCapability("app", app.getAbsolutePath());
       capabilities.setCapability("appPackage", "com.fieldlens.android");
       //	capabilities.setCapability("appActivity", ".activities.DemoActivity");


       AndroidDriver driver = new AndroidDriver (new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
       driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
       return driver;
   }

}
