package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

/**
 * Created by tatyanavolkorezova on 23.01.17.
 */
public class MainScreen {
    protected AndroidDriver driver;

    public MainScreen(AndroidDriver _driver) {
        this.driver = _driver;
    }

    public static String app_package_name = "com.fieldlens.android:id/";
    public static String sign = app_package_name + "signup_button";
    public static String login = app_package_name + "login_button";

    public SignUpPage clickSignUp() {

        driver.findElement(By.id(sign)).click();
        return new SignUpPage(driver);
    }

    public LogInPage clickLogIn() {

        driver.findElement(By.id(login)).click();
        return new LogInPage(driver);
    }


}


