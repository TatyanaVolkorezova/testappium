package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

/**
 * Created by tatyanavolkorezova on 24.01.17.
 */
public class TutorialFirstPage {

    protected AndroidDriver driver;

    public TutorialFirstPage(AndroidDriver _driver) {
        this.driver = _driver;
    }

    public String app_package_name = "com.fieldlens.android:id/";
    public String headerTitle= app_package_name + "header_text";
    public String headerSecond= app_package_name + "secondary_text";
    public String nextBtn= app_package_name + "fragment_company_users_add";

    public UserListInvite clickNextBtn(){

        driver.findElement(By.id(nextBtn)).click();
        return new UserListInvite(driver);

    }

    public void checkIs(){

        if (driver.findElement(By.id(headerTitle)).isDisplayed() && driver.findElement(By.id(headerSecond)).isDisplayed())
            System.out.println("Test passed");
        else
            System.out.println("Test failed");







    }


}
