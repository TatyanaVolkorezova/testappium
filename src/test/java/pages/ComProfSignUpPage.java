package pages;

import TestData.Data;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

/**
 * Created by tatyanavolkorezova on 24.01.17.
 */
public class ComProfSignUpPage {
    protected AndroidDriver driver;

    public ComProfSignUpPage(AndroidDriver _driver)
    {
        this.driver = _driver;
    }

    public String app_package_name = "com.fieldlens.android:id/";
    public String photo= app_package_name + "edit_photo_text";// photo
    public String firstName = app_package_name + "welcome_form_field_first_name";//
    public String lastName = app_package_name + "welcome_form_field_last_name";
    public String phoneNum = app_package_name + "welcome_form_field_phone_number";
    public String jobTitle = app_package_name + "welcome_form_field_job_title";
    public String nextBtn = app_package_name + "welcome_form_action";//


    public CompanyInfoPage clickNextBtn(){
        driver.findElement(By.id(nextBtn)).click();
        return new CompanyInfoPage(driver);
    }

    public CompanyInfoPage addField(){
        //driver.findElement(By.id(photo)).click();
        driver.findElement(By.id(firstName)).sendKeys(Data.firstName);
        driver.findElement(By.id(lastName)).sendKeys(Data.lastName);
        driver.findElement(By.id(phoneNum)).sendKeys(Data.phoneNum);
        driver.findElement(By.id(jobTitle)).sendKeys(Data.jobTitle);
        driver.hideKeyboard();

        return new CompanyInfoPage(driver);

    }




}
