package pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

/**
 * Created by tatyanavolkorezova on 24.01.17.
 */
public class AlertHandle {

    protected AndroidDriver driver;

    public AlertHandle(AndroidDriver _driver){
        this.driver = _driver;
    }

    public static String app_package_name = "com.fieldlens.android:id/";
    public static String popUpGetIn = app_package_name + "dialog_message";


public AlertHandle PopShow(){

    if (driver.findElement(By.id(popUpGetIn)).isDisplayed()){
        System.out.println("ZASHLO");
        return new AlertHandle(driver);

    }
    else
        System.out.println("Pop up isn't shown");

return new AlertHandle(driver);


}

}
