package pages;

import TestData.Data;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

/**
 * Created by tatyanavolkorezova on 19.01.17.
 */
public class SignUpPage {

    protected AndroidDriver driver;

    public SignUpPage(AndroidDriver _driver) {
        this.driver = _driver;
    }

    public String app_package_name = "com.fieldlens.android:id/";
    public String emailField = app_package_name + "welcome_form_field_email";// textfield "Email"
    public String passw = app_package_name + "welcome_form_field_password";// textfield "Password"
    public String nextBtn = app_package_name + "welcome_form_action";//Next btn
    public String  logInBtn = app_package_name + "welcome_form_action_secondary";//LogIn btn
    public String hshBtn = app_package_name + "welcome_form_toggle";//Show/Hide button
    public String footer = app_package_name + "welcome_form_footer_text";//



    public LogInPage clickLogIn() {//click on "LogIn button"

        driver.findElement(By.id(logInBtn)).click();
        return new LogInPage(driver);
    }


    public ComProfSignUpPage clickNext() { //NextButton

        driver.findElement(By.id(nextBtn)).click();
        return new ComProfSignUpPage(driver);
    }

    public AlertHandle AddFields(){
        driver.findElement(By.id(emailField)).sendKeys(Data.testEmail);
        driver.findElement(By.id(passw)).sendKeys(Data.testPassword);
        driver.hideKeyboard();
        return new AlertHandle(driver);
    }
}




//method tap on "Next button"
    // Tap on Hide/Show button (if text=="Hide".. if text=="Show"





