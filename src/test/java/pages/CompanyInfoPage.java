package pages;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by tatyanavolkorezova on 24.01.17.
 */
public class CompanyInfoPage {
    protected AndroidDriver driver;

    public CompanyInfoPage(AndroidDriver _driver) {
        this.driver = _driver;
    }
    public String app_package_name = "com.fieldlens.android:id/";
    public String orgName = app_package_name + "welcome_form_field_company_name";
    public String orgType = app_package_name + "company_type_text";
    public String  getStartBtn = app_package_name + "welcome_form_action";//GetStarted Btn

//public

}
